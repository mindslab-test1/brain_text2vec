FROM tensorflow/tensorflow:1.13.1-gpu-py3

RUN python3 -m pip --no-cache-dir install --upgrade \
        grpcio-tools==1.20.1 \
        sklearn \
        nltk \
        requests \
        tqdm

RUN mkdir /workspace

COPY . /workspace
WORKDIR /workspace

ENV PORT 36000

ENTRYPOINT python server.py --port ${PORT}
