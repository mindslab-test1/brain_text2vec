import grpc
from proto.bert_w2v_pb2 import SimilarWord, Output
from proto.bert_w2v_pb2_grpc import BERT_W2VServicer, add_BERT_W2VServicer_to_server
from concurrent import futures

import argparse
import logging
import time
from pathlib import Path

from preprocess_vectors import get_basic_word_bert_outputs
from bert_w2v import BertW2VEngine


def main():
    args = parse_args()

    w2v_server = BertW2VServer()
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    add_BERT_W2VServicer_to_server(w2v_server, server)
    server.add_insecure_port('[::]:{}'.format(args.port))

    # start server
    server.start()

    logging.basicConfig(
        level=logging.INFO,
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('bert_w2v server starts at 0.0.0.0:{}'.format(args.port))

    try:
        while True:
            time.sleep(60 * 60 * 24)    # one whole day in seconds
    except KeyboardInterrupt:
        server.stop(0)


class BertW2VServer(BERT_W2VServicer):
    def __init__(self):

        # setup server
        print('> Loading saved bert outputs..')
        basic_words_outputs = get_basic_word_bert_outputs()
        print('> Building engine..')
        self.engine = BertW2VEngine(basic_words_outputs)

    def W2V(self, request, context):
        print(request)
        word = request.word

        result = self.engine.query(word)

        similar_words = [SimilarWord(word=word, distance=dist) for word, dist, _ in result]
        output = Output(similar_words=similar_words)

        return output


# define and read arguments especially for running, not modeling
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, required=True)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
