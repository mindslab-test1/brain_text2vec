import re
import nltk
import unicodedata
import numpy as np
# import matplotlib.pyplot as plt


def _is_whitespace(c):
    if c == " " or c == "\t" or c == "\r" or c == "\n" or ord(c) == 0x202F:
        return True
    cat = unicodedata.category(c)
    if cat == "Zs":
        return True
    return False


def _is_control(char):
  """Checks whether `chars` is a control character."""
  # These are technically control characters but we count them as whitespace
  # characters.
  if char == "\t" or char == "\n" or char == "\r":
    return False
  cat = unicodedata.category(char)
  if cat.startswith("C"):
    return True
  return False


def clean_text(text):
    """Performs invalid character removal and whitespace cleanup on text."""
    output = []
    for char in text:
      cp = ord(char)
      if cp == 0 or cp == 0xfffd or _is_control(char):
        continue
      if _is_whitespace(char):
        output.append(" ")
      else:
        output.append(char)
    return "".join(output)


def is_punctuation(char):
  """Checks whether `chars` is a punctuation character."""
  cp = ord(char)
  # We treat all non-letter/number ASCII as punctuation.
  # Characters such as "^", "$", and "`" are not in the Unicode
  # Punctuation class but we treat them as punctuation anyways, for
  # consistency.
  if ((cp >= 33 and cp <= 47) or (cp >= 58 and cp <= 64) or
      (cp >= 91 and cp <= 96) or (cp >= 123 and cp <= 126)):
    return True
  cat = unicodedata.category(char)
  if cat.startswith("P"):
    return True
  return False


def zng(paragraph):
    for sent in re.findall(u'[^!?。\.\!\?]+[!?。\.\!\?]?', paragraph, flags=re.U):
        yield sent


jp_sent_tokenizer = nltk.RegexpTokenizer('[^ 「」!?。．）]*[!?。]')


def convert_text(text):
   '''
   convert text for sentence split
   split token: '.. '
   '''
   try:
       detected = detect(text)
   except:
       detected = 'else'

   if detected == 'zh-cn':
       out = list(zng(text))
   elif detected == 'ru':
       out = nltk.sent_tokenize(text, language="russian")
   elif detected == 'en':
       out = nltk.sent_tokenize(text, language="english")
   elif detected == 'ja':
       out = jp_sent_tokenizer.tokenize(text)
   else:
       out = re.sub('[\n]+', '',
                    re.sub('[?]', '?.. ',
                           re.sub('다\.', '다... ',
                                  re.sub('까\.', '까... ',
                                         re.sub('\."', '"',
                                                re.sub('[!]', '!.. ',
                                                       re.sub('[.?]+', '.',
                                                              re.sub('[ ]+', ' ',
                                                                     re.sub('[.]+', '.', text)))))))))
       out = out.split('.. ')

   return out


def rm_sp(x):
    while 1:
        try:
            x.remove('')
        except:
            try:
                x.remove(' ')
            except:
                break
    return x


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)


def tokenizer_encode(passage, tokenizer):
        doc_tokens = []
        char_to_word_offset = []
        prev_is_whitespace = True
        for c in passage:
            if _is_whitespace(c):
                prev_is_whitespace = True
                doc_tokens.append(' ')
            else:
                if prev_is_whitespace:
                    doc_tokens.append(c)
                else:
                    doc_tokens[-1] += c
                prev_is_whitespace = False
            char_to_word_offset.append(len(doc_tokens) - 1)

        tok_to_orig_index = []
        orig_to_tok_index = []
        sub_tokens_length = []
        all_doc_tokens = []
        for (i, token) in enumerate(doc_tokens):
            orig_to_tok_index.append(len(all_doc_tokens))
            sub_tokens = tokenizer.tokenize(token)
            if '[UNK]' in sub_tokens:
                try:
                    if detect(passage) != 'zh-cn':
                        sub_tokens = [token]
                except:
                    sub_tokens = [token]
            if sub_tokens == []:
                try:
                    sub_tokens_length[-1] += 1
                except:
                    sub_tokens_length.append(1)
            else:
                for sub_token in sub_tokens:
                    tok_to_orig_index.append(i)
                    if sub_token == '[UNK]':
                        sub_tokens_length.append(1)
                    else:
                        sub_tokens_length.append(len(sub_token.replace(' ##', '').replace('##', '')))
                    all_doc_tokens.append(sub_token)

        return all_doc_tokens, sub_tokens_length


# def plot_confusion_matrix(y_true, y_pred, output_dir,
#                           classes=None,
#                           normalize=False,
#                           title=None,
#                           cmap=plt.cm.Blues):
#     """
#     This function prints and plots the confusion matrix.
#     Normalization can be applied by setting `normalize=True`.
#     """
#     if not title:
#         if normalize:
#             title = 'Normalized confusion matrix'
#         else:
#             title = 'Confusion matrix, without normalization'
#
#     # Compute confusion matrix
#     cm = confusion_matrix(y_true, y_pred)
#     # Only use the labels that appear in the data
#     if classes is not None:
#         classes = classes[unique_labels(y_true, y_pred)]
#     else:
#         classes = unique_labels(y_true, y_pred)
#     if normalize:
#         cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
#         print("Normalized confusion matrix")
#     else:
#         print('Confusion matrix, without normalization')
#
#     print('ploting confusion matrix')
#
#     fig, ax = plt.subplots()
#     im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
#     ax.figure.colorbar(im, ax=ax)
#     # We want to show all ticks...
#     ax.set(xticks=np.arange(cm.shape[1]),
#            yticks=np.arange(cm.shape[0]),
#            # ... and label them with the respective list entries
#            xticklabels=classes, yticklabels=classes,
#            title=title,
#            ylabel='True label',
#            xlabel='Predicted label')
#
#     # Rotate the tick labels and set their alignment.
#     plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
#              rotation_mode="anchor")
#
#     # Loop over data dimensions and create text annotations.
#     fmt = '.2f' if normalize else 'd'
#     thresh = cm.max() / 2.
#     for i in range(cm.shape[0]):
#         for j in range(cm.shape[1]):
#             ax.text(j, i, format(cm[i, j], fmt),
#                     ha="center", va="center",
#                     color="white" if cm[i, j] > thresh else "black")
#     fig.tight_layout()
#     plt.savefig(output_dir)
#     return ax
