import requests
import pickle
import numpy as np
import random
from xml.etree import ElementTree
from tqdm import tqdm
from pathlib import Path

from feat_extract import BertFeat

def get_krdict():
    pickle_path = Path('data/krdict_items.pkl')

    if not pickle_path.exists():
        url_template = 'http://175.125.91.94/oasis/service/rest/meta12/getKRAG04121?numOfRows={}&pageNo={}'

        rows = 100
        total = 51603

        items = []
        for rows_idx in tqdm(range(0, total, rows), desc='API requests...'):
            url = url_template.format(rows, rows_idx//rows + 1)
            response = requests.get(url)
            tree = ElementTree.fromstring(response.content)
            body = tree[1]          # 0: header, 1: body
            items.extend(body[0])   # 0: items, 1: numOfRows, 2: pageNo, 3: totalCount

        with pickle_path.open('wb') as f:
            pickle.dump(items, f)
    else:
        with pickle_path.open('rb') as f:
            items = pickle.load(f)

    word_descs = []
    words_check = {}
    for item in items:              # 0: collectionDb, 1: creator, 2: description: 3: language, 4: title
        word = item[4].text
        desc = item[2].text
        if word in words_check:
            continue
        else:
            words_check[word] = True
        word_descs.append((word, desc))

    return word_descs


''' krdict description example:
1.중학교를 졸업한 수준의 학력을 갖추거나 중학교를 졸업하면 갈 수 있는 학교.\n1.\n<구> 남자 고등학교.\n<구> 여자 고등학교.\n<구> 인문계 고등학교.\n<구> 특수 목적 고등학교.\n<구> 사립 고등학교.\n<구> 일류 고등학교.\n<구> 고등학교 동창생.\n<구> 고등학교 졸업.\n<구> 고등학교 친구.\n<구> 고등학교를 가다.\n<구> 고등학교를 나오다.\n<구> 고등학교를 다니다.\n<구> 고등학교를 마치다.\n<구> 고등학교를 졸업하다.\n<구> 고등학교에 들어가다.\n<구> 고등학교에 올라가다.\n<구> 고등학교에 입학하다.\n<구> 고등학교에 진학하다.\n<구> 고등학교에 합격하다.\n<문장> 승규는 고등학교를 수석으로 졸업하고 의대에 입학했다.\n<문장> 지수는 중학교를 졸업한 후 집에서 가까운 고등학교에 진학할 예정이다.\n<대화> 가: 너는 고등학교에 들어가면 문과, 이과 중 어디를 택할거야?\n<대화> 나: 나는 수학을 못하니까 문과를 택할거야.\n\n
'''
def _process_description(desc):
    _desc = desc.split('\n')[0]
    if _desc.startswith('1.'):
        _desc = _desc[2:]
    return _desc

# return: list of dicts of {word, description, layers_out, token_length}
def get_basic_word_desc_bert_outputs():
    pickle_path = Path('data/krdict_word_desc_seq_vectors.pkl')

    if not pickle_path.exists():
        model = BertFeat(config='./models/lm_config.json')

        word_descs = get_krdict()

        batch_size = 200
        step = len(word_descs) // 10
        _borders = list(range(0, len(word_descs), step)) + [len(word_descs)]
        borders = list(zip(_borders[:-1], _borders[1:]))
        random.shuffle(borders)
        print(borders)

        all_outputs = []
        for border_no, (beg, end) in enumerate(borders):
            partial_outputs = []
            for idx in tqdm(range(beg, end, batch_size), desc='Extracting outputs.. {}'.format(border_no)):
                _word_descs = word_descs[idx:idx+batch_size]
                _words = [word for word, desc in _word_descs]
                _descs = [_process_description(desc) for word, desc in _word_descs]

                word_outs = model.get_feat(_words, seq_out=False)
                desc_outs = model.get_feat(_descs, seq_out=False)
                for (word, desc), (word_out, word_token_len), (desc_out, desc_token_len) in zip(_word_descs, word_outs, desc_outs):
                    partial_outputs.append({
                        'word': word,
                        'desc': _process_description(desc),
                        'word_out': word_out,
                        'desc_out': desc_out,
                        'word_token_len': word_token_len,
                        'desc_token_len': desc_token_len,
                    })

            with pickle_path.with_suffix('.{}.pkl'.format(border_no)).open('wb') as f:
                pickle.dump(partial_outputs, f)

            # all_outputs.extend(partial_outputs)
            del partial_outputs

        with pickle_path.open('wb') as f:
            pickle.dump(all_outputs, f)
    else:
        # TODO
        with pickle_path.open('rb') as f:
            all_outputs = pickle.load(f)

    return all_outputs

def get_basic_word_bert_outputs():
    pickle_path = Path('data/krdict_word_seq_vectors.pkl')

    if not pickle_path.exists():
        model = BertFeat(config='./models/lm_config.json')

        word_descs = get_krdict()

        batch_size = 200
        step = len(word_descs) // 10
        _borders = list(range(0, len(word_descs), step)) + [len(word_descs)]
        borders = list(zip(*[_borders[:-1], _borders[1:]]))

        all_outputs = []
        for border_no, (beg, end) in enumerate(borders):
            partial_outputs = []
            for idx in tqdm(range(beg, end, batch_size), desc='Extracting outputs.. {}'.format(border_no)):
                _word_descs = word_descs[idx:idx+batch_size]
                _words = [word for word, desc in _word_descs]

                outs = model.get_feat(_words)
                for (word, desc), (layers_out, token_length) in zip(_word_descs, outs):
                    partial_outputs.append({
                        'word': word,
                        'description': desc,
                        'layers_out': layers_out,
                        'token_length': token_length
                    })

            with pickle_path.with_suffix('.{}.pkl'.format(border_no)).open('wb') as f:
                pickle.dump(partial_outputs, f)

            all_outputs.extend(partial_outputs)

        with pickle_path.open('wb') as f:
            pickle.dump(all_outputs, f)
    else:
        # TODO
        with pickle_path.open('rb') as f:
            all_outputs = pickle.load(f)

    return all_outputs


if __name__ == '__main__':
    words = get_krdict()
    # get_basic_word_bert_outputs()
    get_basic_word_desc_bert_outputs()

