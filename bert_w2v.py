# referenced: gist.github.com/avidale/c6b19687d333655da483421880441950
import pickle
import numpy as np
from tqdm import tqdm
from pathlib import Path
from sklearn.neighbors import KDTree

from preprocess_vectors import get_basic_word_bert_outputs
from feat_extract import BertFeat

class BertW2VEngine(object):
    def __init__(self, base_outputs):
        self.base_outputs = base_outputs

        self.idx_word = []
        self.idx_embedding = []
        self.idx_description = []
        self.idx_token_length = []

        for out_dict in base_outputs:
            self.idx_word.append(out_dict['word'])
            self.idx_description.append(out_dict['description'])
            self.idx_token_length.append(out_dict['token_length'])

            emb = self._choose_embedding(out_dict['layers_out'], token_length=out_dict['token_length'])
            self.idx_embedding.append(emb)

        self.word_idx = {word:idx for idx, word in enumerate(self.idx_word)}

        self._build_search_index()

        print('> Loading trained bert model')
        self.model = BertFeat(config='models/lm_config.json')
        self.model.get_feat(['warm up'])

    # 'last', 'embedding', 'last-n'
    # 'cls', 'first', 'tokens'
    def _choose_embedding(self, layers_out, token_length=1, layer_choice='last', token_choice='cls'):
        layer_choice_dict = {
            'embedding': 0,
            'last': -1,
        }
        token_choice_dict = {
            'cls': 0,
            'first': 1,
        }

        layer_idx = layer_choice_dict[layer_choice]
        token_idx = token_choice_dict[token_choice]
        
        return layers_out[layer_idx, token_idx, :]

    def _build_search_index(self):
        all_embeddings = np.stack(self.idx_embedding)
        self.normed_embeddings = (all_embeddings.T / (all_embeddings**2).sum(axis=1) ** 0.5).T
        self.indexer = KDTree(self.normed_embeddings)

    def query(self, query_word, k=10):
        if query_word in self.word_idx:
            query_emb = self.idx_embedding[self.word_idx[query_word]]
        else:
            print('> {} is a new word. Processing its embedding'.format(query_word))
            layers_out, token_length = self.model.get_feat([query_word])[0]
            query_emb = self._choose_embedding(layers_out, token_length=token_length)

        dist, index = self.indexer.query(query_emb.reshape(1, -1), k=k)

        distances = []
        neighbors = []
        descriptions = []

        for idx, dist in zip(index.ravel(), dist.ravel()):
            word = self.idx_word[idx]

            if word == query_word:
                continue

            distances.append(dist)
            neighbors.append(word)
            descriptions.append(self.idx_description[idx])

            if len(distances) >= k:
                break

        return zip(neighbors, distances, descriptions)


if __name__ == '__main__':
    # list of dicts of {word, description, layers_out, token_length}
    print('> Loading saved bert outputs..')
    basic_words_outputs = get_basic_word_bert_outputs()
    print('> Building engine..')
    engine = BertW2VEngine(basic_words_outputs)

    while True:
        query_word = ''
        while query_word == '':
            query_word = input('query: ')

        result = engine.query(query_word)
        for word, dist, desc in result:
            print('{} ({})'.format(word, dist))
