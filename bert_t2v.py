# referenced: gist.github.com/avidale/c6b19687d333655da483421880441950
import numpy as np
from tqdm import tqdm
from pathlib import Path
from sklearn.neighbors import KDTree

from preprocess_vectors import get_basic_word_bert_outputs, get_basic_word_desc_bert_outputs
from feat_extract import BertFeat

class BertW2VEngine(object):
    def __init__(self, base_outputs):
        self.base_outputs = base_outputs

        self.idx_word = []
        self.idx_word_embedding = []
        self.idx_word_token_len = []
        self.idx_desc = []
        self.idx_desc_embedding = []
        self.idx_desc_token_len = []

        for out_dict in base_outputs:
            self.idx_word.append(out_dict['word'])
            self.idx_desc.append(out_dict['desc'])
            self.idx_word_token_len.append(out_dict['word_token_len'])
            self.idx_desc_token_len.append(out_dict['desc_token_len'])

            word_emb = self._choose_embedding(out_dict['word_out'], token_length=out_dict['word_token_len'])
            desc_emb = self._choose_embedding(out_dict['desc_out'], token_length=out_dict['desc_token_len'])
            self.idx_word_embedding.append(word_emb)
            self.idx_desc_embedding.append(desc_emb)

        self.word_idx = {word:idx for idx, word in enumerate(self.idx_word)}
        # self.desc_idx = {desc:idx for idx, desc in enumerate(self.idx_desc)}

        self.word_indexer = self._build_search_index(self.idx_word_embedding)
        self.desc_indexer = self._build_search_index(self.idx_desc_embedding)

        print('> Loading trained bert model')
        self.model = BertFeat(config='models/lm_config.json')
        self.model.get_feat(['warm up'])

    # 'last', 'embedding', 'last-n'
    # 'cls', 'first', 'tokens'
    def _choose_embedding(self, layers_out, token_length=1, layer_choice='last', token_choice='cls'):
        layer_choice_dict = {
            'embedding': 0,
            'last': -1,
        }
        token_choice_dict = {
            'cls': 0,
            'first': 1,
        }

        layer_idx = layer_choice_dict[layer_choice]
        token_idx = token_choice_dict[token_choice]
        
        return layers_out[layer_idx, token_idx, :]

    def _build_search_index(self, embeddings):
        all_embeddings = np.stack(embeddings)
        normed_embeddings = (all_embeddings.T / (all_embeddings**2).sum(axis=1) ** 0.5).T
        indexer = KDTree(normed_embeddings)

        return indexer

    def query(self, query_text, k=10, get_desc=False):
        if query_text in self.word_idx:
            query_emb = self.idx_word_embedding[self.word_idx[query_text]]
        else:
            print('> {} is a new text. Processing its embedding'.format(query_text))
            layers_out, token_length = self.model.get_feat([query_text])[0]
            query_emb = self._choose_embedding(layers_out, token_length=token_length)

        indexer = self.desc_indexer if get_desc else self.word_indexer
        dist, index = indexer.query(query_emb.reshape(1, -1), k=k)

        distances = []
        neighbors = []
        descriptions = []

        for idx, dist in zip(index.ravel(), dist.ravel()):
            word = self.idx_word[idx]

            if word == query_text:
                continue

            distances.append(dist)
            neighbors.append(word)
            descriptions.append(self.idx_desc[idx])

            if len(distances) >= k:
                break

        return zip(neighbors, distances, descriptions)


if __name__ == '__main__':
    # list of dicts of {word, description, layers_out, token_length}
    print('> Loading saved bert outputs..')
    # basic_words_outputs = get_basic_word_bert_outputs()
    basic_word_desc_outputs = get_basic_word_desc_bert_outputs()
    print('> Building engine..')
    engine = BertW2VEngine(basic_word_desc_outputs)

    while True:
        query_text = ''
        while query_text == '':
            query_text = input('query: ')

        print('> Description search result: ')
        desc_result = engine.query(query_text, get_desc=True)
        for word, dist, desc in desc_result:
            print('{}  :[{}]'.format(desc, word, dist))

        word_result = engine.query(query_text, get_desc=False)
        print('> Word search result: ')
        for word, dist, desc in word_result:
            print('[{}]: {}'.format(word, desc, dist))

        print('=' * 20)
