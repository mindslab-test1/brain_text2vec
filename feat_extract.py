import os
import sys
import json
import collections
import tensorflow as tf
import numpy as np

from core import modeling
from core import tokenization
from core import featExt_core

import time


class BertFeat:
    def __init__(self, config, max_seq_length=512, init_checkpoint=None, is_training=False):
        if type(config) == str:
            config = json.load(open(config))
        else:
            config = config

        self.config = config
        self.is_training = is_training
        self.bert_config = modeling.BertConfig.from_json_file(config['bert_config'])

        print('loaded ckpt graph')
        if max_seq_length > 512:
            raise ValueError('sequence length less than 512')
        self.max_seq_length = max_seq_length
        self.input_ids = tf.placeholder(tf.int32, shape=[None, max_seq_length], name='input_ids')
        self.input_mask = tf.placeholder(tf.int32, shape=[None, max_seq_length], name='input_mask')
        self.segment_ids = tf.placeholder(tf.int32, shape=[None, max_seq_length], name='segment_ids')

        model = modeling.BertModel(
            config=self.bert_config,
            is_training=is_training,
            input_ids=self.input_ids,
            input_mask=self.input_mask,
            token_type_ids=self.segment_ids,
            use_one_hot_embeddings=False,
            vocab_size=None)

        self.pooled_out = model.get_pooled_output()
        self.seq_out = model.get_sequence_output()
        self.all_out = model.get_all_encoder_layers()
        self.emb_out = model.get_embedding_output()
        self.whole_out = model.get_whole_output()

        tvars = tf.trainable_variables()

        initialized_variable_names = {}
        if init_checkpoint is None:
            init_checkpoint = config['init_checkpoint']

        if init_checkpoint:
            (assignment_map, initialized_variable_names) = \
                modeling.get_assignment_map_from_checkpoint(tvars, init_checkpoint)
            tf.train.init_from_checkpoint(init_checkpoint, assignment_map)

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True)))
        self.sess.run(tf.global_variables_initializer())
        self.sess.run(self.pooled_out, feed_dict={self.input_ids: [[0 for _ in range(self.input_ids.shape[-1])]],
                                                  self.input_mask: [[0 for _ in range(self.input_mask.shape[-1])]],
                                                  self.segment_ids: [[0 for _ in range(self.segment_ids.shape[-1])]]})

        self.tokenizer = tokenization.FullTokenizer(vocab_file=config['vocab_file'],
                                                    do_lower_case=config['do_lower_case'])
        self.processor = featExt_core.XdcProcessor()

    def get_feat(self, input_doc, seq_out=True):
        if type(input_doc) == list:
            examples = self.processor._create_examples(input_doc, set_type='perd', labels=None)
        else:
            examples = self.processor._create_examples([input_doc], set_type='perd', labels=None)

        eval_features = []
        for (ex_index, example) in enumerate(examples):
            feature = featExt_core.convert_single_example(
                ex_index, example, self.max_seq_length, self.tokenizer, True, 30)

            features = collections.OrderedDict()
            features["input_ids"] = feature.input_ids
            features["input_mask"] = feature.input_mask
            features["segment_ids"] = feature.segment_ids
            eval_features.append(features)

        feed_dict = {self.input_ids: [features['input_ids'] for features in eval_features],
                     self.input_mask: [features['input_mask'] for features in eval_features],
                     self.segment_ids: [features['segment_ids'] for features in eval_features]}

        '''
        if seq_out:
            out = self.sess.run(self.seq_out, feed_dict=feed_dict)
        else:
            out = self.sess.run(self.pooled_out, feed_dict=feed_dict)
        '''
        outs = self.sess.run(self.whole_out, feed_dict=feed_dict)  # list 0: embedding. 1~12: layer output
        outs = np.stack(outs, axis=1)
        token_lengths = [sum(feature['input_mask']) - 2 for feature in eval_features] # [CLS], [SEP]

        result = []
        for out, token_length in zip(outs, token_lengths):
            out_ = out[:, :token_length+2, :] if seq_out else out[:, :1, :]
            result.append((out_, token_length))

        return result


if __name__ == '__main__':
    ff = BertFeat(config='./models/lm_config.json', max_seq_length=512)

    input_doc = ['기억 장치', '대한민국 마인즈랩 만세']
    outs = ff.get_feat(input_doc, seq_out=True)

    for vector, token_length in outs:
        print(vector.shape, token_length)
