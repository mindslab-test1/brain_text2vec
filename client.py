import grpc
from proto.bert_w2v_pb2 import Input
from proto.bert_w2v_pb2_grpc import BERT_W2VStub

import argparse


def main():
    args = parse_args()

    client = BertW2VClient('{}:{}'.format(args.server, args.port))
    output = client.W2V(args.word)

    print(output)

class BertW2VClient(object):
    def __init__(self, remote):
        channel = grpc.insecure_channel(remote)
        self.stub = BERT_W2VStub(channel)

    def W2V(self, word):
        w2v_input = Input(word=word)
        return self.stub.W2V(w2v_input)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--server', type=str, default='localhost')
    parser.add_argument('--port', type=int, required=True)
    parser.add_argument('--word', type=str)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    main()
