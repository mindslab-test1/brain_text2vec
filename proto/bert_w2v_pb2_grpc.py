# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from proto import bert_w2v_pb2 as proto_dot_bert__w2v__pb2


class BERT_W2VStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.W2V = channel.unary_unary(
        '/maum.brain.bert_w2v.BERT_W2V/W2V',
        request_serializer=proto_dot_bert__w2v__pb2.Input.SerializeToString,
        response_deserializer=proto_dot_bert__w2v__pb2.Output.FromString,
        )


class BERT_W2VServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def W2V(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_BERT_W2VServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'W2V': grpc.unary_unary_rpc_method_handler(
          servicer.W2V,
          request_deserializer=proto_dot_bert__w2v__pb2.Input.FromString,
          response_serializer=proto_dot_bert__w2v__pb2.Output.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'maum.brain.bert_w2v.BERT_W2V', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
