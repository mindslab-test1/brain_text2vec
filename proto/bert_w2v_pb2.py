# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/bert_w2v.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='proto/bert_w2v.proto',
  package='maum.brain.bert_w2v',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x14proto/bert_w2v.proto\x12\x13maum.brain.bert_w2v\"\x15\n\x05Input\x12\x0c\n\x04word\x18\x01 \x01(\t\"-\n\x0bSimilarWord\x12\x0c\n\x04word\x18\x01 \x01(\t\x12\x10\n\x08\x64istance\x18\x02 \x01(\x02\"A\n\x06Output\x12\x37\n\rsimilar_words\x18\x01 \x03(\x0b\x32 .maum.brain.bert_w2v.SimilarWord2L\n\x08\x42\x45RT_W2V\x12@\n\x03W2V\x12\x1a.maum.brain.bert_w2v.Input\x1a\x1b.maum.brain.bert_w2v.Output\"\x00\x62\x06proto3')
)




_INPUT = _descriptor.Descriptor(
  name='Input',
  full_name='maum.brain.bert_w2v.Input',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='word', full_name='maum.brain.bert_w2v.Input.word', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=45,
  serialized_end=66,
)


_SIMILARWORD = _descriptor.Descriptor(
  name='SimilarWord',
  full_name='maum.brain.bert_w2v.SimilarWord',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='word', full_name='maum.brain.bert_w2v.SimilarWord.word', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='distance', full_name='maum.brain.bert_w2v.SimilarWord.distance', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=68,
  serialized_end=113,
)


_OUTPUT = _descriptor.Descriptor(
  name='Output',
  full_name='maum.brain.bert_w2v.Output',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='similar_words', full_name='maum.brain.bert_w2v.Output.similar_words', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=115,
  serialized_end=180,
)

_OUTPUT.fields_by_name['similar_words'].message_type = _SIMILARWORD
DESCRIPTOR.message_types_by_name['Input'] = _INPUT
DESCRIPTOR.message_types_by_name['SimilarWord'] = _SIMILARWORD
DESCRIPTOR.message_types_by_name['Output'] = _OUTPUT
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Input = _reflection.GeneratedProtocolMessageType('Input', (_message.Message,), dict(
  DESCRIPTOR = _INPUT,
  __module__ = 'proto.bert_w2v_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert_w2v.Input)
  ))
_sym_db.RegisterMessage(Input)

SimilarWord = _reflection.GeneratedProtocolMessageType('SimilarWord', (_message.Message,), dict(
  DESCRIPTOR = _SIMILARWORD,
  __module__ = 'proto.bert_w2v_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert_w2v.SimilarWord)
  ))
_sym_db.RegisterMessage(SimilarWord)

Output = _reflection.GeneratedProtocolMessageType('Output', (_message.Message,), dict(
  DESCRIPTOR = _OUTPUT,
  __module__ = 'proto.bert_w2v_pb2'
  # @@protoc_insertion_point(class_scope:maum.brain.bert_w2v.Output)
  ))
_sym_db.RegisterMessage(Output)



_BERT_W2V = _descriptor.ServiceDescriptor(
  name='BERT_W2V',
  full_name='maum.brain.bert_w2v.BERT_W2V',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=182,
  serialized_end=258,
  methods=[
  _descriptor.MethodDescriptor(
    name='W2V',
    full_name='maum.brain.bert_w2v.BERT_W2V.W2V',
    index=0,
    containing_service=None,
    input_type=_INPUT,
    output_type=_OUTPUT,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_BERT_W2V)

DESCRIPTOR.services_by_name['BERT_W2V'] = _BERT_W2V

# @@protoc_insertion_point(module_scope)
